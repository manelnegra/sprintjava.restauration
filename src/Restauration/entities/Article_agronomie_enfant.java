/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Restauration.entities;

import java.util.Date;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author WIKI
 */
public class Article_agronomie_enfant {

    private String sujetArticle;
    private Date dateCreationArticle;
    private String sourceArticle;
    private String titreArticle;
    private String ContenuArticle;

    public Article_agronomie_enfant() {//To change body of generated methods, choose Tools | Templates.
    }

    public void setSujetArticle(String sujetArticle) {
        this.sujetArticle = sujetArticle;
    }

    public String getSujetArticle() {
        return sujetArticle;
    }

    public void setSourceArticle(String sourceArticle) {
        this.sourceArticle = sourceArticle;
    }

    public String getSourceArticle() {
        return sourceArticle;
    }

    public void setTitreArticle(String titreArticle) {
        this.titreArticle = titreArticle;
    }

    public String getTitreArticle() {
        return titreArticle;
    }

    public void setDateCreationArticle(Date dateCreationArticle) {
        this.dateCreationArticle = dateCreationArticle;
    }

    public Date getDateCreationArticle() {
        return dateCreationArticle;
    }

    public void setContenuArticle(String ContenuArticle) {
        this.ContenuArticle = ContenuArticle;
    }

    public String getContenuArticle() {
        return ContenuArticle;
    }

    public Article_agronomie_enfant(String sujetArticle, Date dateCreationArticle, String sourceArticle, String titreArticle, String ContenuArticle) {
        this.sujetArticle = sujetArticle;
        this.sourceArticle = sourceArticle;
        this.titreArticle = titreArticle;
        this.dateCreationArticle = dateCreationArticle;
        this.ContenuArticle = ContenuArticle;

    }

}
