/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Restauration.entities;

import java.util.Date;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Fabio
 */
public class Plat {
    
    
    private int id ;
    private String nomPlat ;
    private Date dateDispo;
    private Float prixPlat;
    private String detailsPlat;


  
    
     public Plat() {//To change body of generated methods, choose Tools | Templates.
    }
     public int getId(){
        return id;}
 
public void setNomPlat(String nomPlat) {
        this.nomPlat =  nomPlat;
    }
public String getNomPlat(){
        return nomPlat;}



public void setDetailsPlat(String detailsPlat) {
        this.detailsPlat =  detailsPlat;
    }
public String getDetailsPlat(){
        return detailsPlat;}

public void setPrixPlat(Float prixPlat) {
        this.prixPlat =  prixPlat;
    }
public Float getPrixPlat(){
        return prixPlat;}


public void setDateDispo(Date dateDispo) {
        this.dateDispo =  dateDispo;
    }


public Date getDateDispo() {
        return dateDispo;
    }




 
    

    

    



    public Plat(String nomPlat, Date dateDispo, Float prixPlat,String detailsPlat ) {
        this.nomPlat = nomPlat;
        this.detailsPlat = detailsPlat;
        this.prixPlat = prixPlat;
        this.dateDispo = dateDispo;
   
    }
    
    
    
  
    
}
