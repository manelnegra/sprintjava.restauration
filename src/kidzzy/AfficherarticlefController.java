/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kidzzy;

import Restauration.entities.Article_agronomie_enfant;
import crud.Article_agronomie_enfantCrud;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author WIKI
 */
public class AfficherarticlefController implements Initializable {

    @FXML
    private TableView<Article_agronomie_enfant> tablev;
    @FXML
    private TableColumn<Article_agronomie_enfant, String> sujet;
    @FXML
    private TableColumn<Article_agronomie_enfant, Date> date;
    @FXML
    private TableColumn<Article_agronomie_enfant, String> source;
    @FXML
    private TableColumn<Article_agronomie_enfant, String> titre;
    @FXML
    private TableColumn<Article_agronomie_enfant, String> contenu;
    @FXML
    private TextField rech;
        private final ObservableList<Article_agronomie_enfant> datta = FXCollections.observableArrayList();
    @FXML
    private Button btnretour;

    /**
     * Initializes the controller class.
     */
        private void settable() {

        Article_agronomie_enfantCrud pl = new Article_agronomie_enfantCrud();

        ArrayList<Article_agronomie_enfant> p = (ArrayList<Article_agronomie_enfant>) pl.displayPlat();
        ObservableList<Article_agronomie_enfant> obs = FXCollections.observableArrayList(p);
        tablev.setItems(obs);
        sujet.setCellValueFactory(new PropertyValueFactory<Article_agronomie_enfant, String>("sujetArticle"));
        date.setCellValueFactory(new PropertyValueFactory<Article_agronomie_enfant, Date>("dateCreationArticle"));
        source.setCellValueFactory(new PropertyValueFactory<Article_agronomie_enfant, String>("sourceArticle"));
        titre.setCellValueFactory(new PropertyValueFactory<Article_agronomie_enfant, String>("titreArticle"));
        contenu.setCellValueFactory(new PropertyValueFactory<Article_agronomie_enfant, String>("ContenuArticle"));
        datta.addAll(p);

    }
     
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       settable();
        FilteredList<Article_agronomie_enfant> filteredData = new FilteredList<>(datta, b -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        rech.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(Article_agronomie_enfant -> {
                // If filter text is empty, display all persons.

                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (Article_agronomie_enfant.getTitreArticle().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true; // Filter matches first name.
                }  else {
                    return false; // Does not match.
                }
            });
        });

        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<Article_agronomie_enfant> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        // 	  Otherwise, sorting the TableView would have no effect.
        sortedData.comparatorProperty().bind(tablev.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        tablev.setItems(sortedData);
    }  
    
    @FXML
    private void retour(ActionEvent event) {
         try {
            Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
            Stage stage = (Stage) btnretour.getScene().getWindow();
            stage.close();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(AfficherarticlebController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
}
