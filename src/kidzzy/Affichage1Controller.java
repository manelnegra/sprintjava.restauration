/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kidzzy;

import Restauration.entities.Plat;
import crud.PlatCrud;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author WIKI
 */
public class Affichage1Controller implements Initializable {

    @FXML
    private TableView<Plat> tablev;
    @FXML
    private TableColumn<Plat, String> nom;
    @FXML
    private TableColumn<Plat, Float> prix;
    @FXML
    private TableColumn<Plat, Date> date;
    @FXML
    private TableColumn<Plat, String> details;
    @FXML
    private TextField rech;
    private final ObservableList<Plat> datta = FXCollections.observableArrayList();
    @FXML
    private Button btnretour;

    /**
     * Initializes the controller class.
     */
    private void settable() {

        PlatCrud pl = new PlatCrud();

        ArrayList<Plat> p = (ArrayList<Plat>) pl.displayPlat();
        ObservableList<Plat> obs = FXCollections.observableArrayList(p);
        tablev.setItems(obs);
        nom.setCellValueFactory(new PropertyValueFactory<Plat, String>("nomPlat"));
        prix.setCellValueFactory(new PropertyValueFactory<Plat, Float>("prixPlat"));
        date.setCellValueFactory(new PropertyValueFactory<Plat, Date>("dateDispo"));
        details.setCellValueFactory(new PropertyValueFactory<Plat, String>("detailsPlat"));
        datta.addAll(p);

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        settable();
        FilteredList<Plat> filteredData = new FilteredList<>(datta, b -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        rech.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(Plat -> {
                // If filter text is empty, display all persons.

                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (Plat.getNomPlat().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true; // Filter matches first name.
                } else {
                    return false; // Does not match.
                }
            });
        });

        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<Plat> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        // 	  Otherwise, sorting the TableView would have no effect.
        sortedData.comparatorProperty().bind(tablev.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        tablev.setItems(sortedData);

    }

    @FXML
    private void retour(ActionEvent event) {
           try {
            Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
            Stage stage = (Stage) btnretour.getScene().getWindow();
            stage.close();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
