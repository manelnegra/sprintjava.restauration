/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kidzzy;

import Restauration.entities.Plat;
import crud.PlatCrud;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author WIKI
 */
public class EditplatController implements Initializable {

    @FXML
    private Button btnedit;
    @FXML
    private Button btnret;
    @FXML
    private TextField nomedit;
    @FXML
    private DatePicker dateedit;
    @FXML
    private TextField prixedit;
    @FXML
    private TextArea detailedit;
    
    String nom_recup = AffichageController.nomrecup;
    Float prix_recup = AffichageController.prixrecup;
    String detail_recup = AffichageController.detailsrecup;
    

    void cleardata() {
        nomedit.clear();
        prixedit.clear();
        detailedit.clear();
       
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
          nomedit.setText(nom_recup);
        prixedit.setText(String.valueOf(prix_recup));
        detailedit.setText(detail_recup);
        
    }

    @FXML
    private void edit(ActionEvent event) {
        String Nom = nomedit.getText();
        Float Prix = Float.parseFloat(prixedit.getText());

        LocalDate Dateplat = dateedit.getValue();
        Date daten = Date.valueOf(Dateplat);
        String Details = detailedit.getText();

        Plat p = new Plat(Nom, daten, Prix, Details);
        PlatCrud udao = PlatCrud.getInstance();
        udao.update(p);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Modification Plat");
        alert.setContentText("Plat a ete modifier avec succes!");

        alert.showAndWait();
        cleardata();
    }

    @FXML
    private void retour(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("affichageAdmin.fxml"));
            Stage stage = (Stage) btnret.getScene().getWindow();
            stage.close();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(EditplatController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
