/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kidzzy;

import Restauration.entities.Article_agronomie_enfant;
import Restauration.entities.Plat;
import crud.Article_agronomie_enfantCrud;
import crud.PlatCrud;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author WIKI
 */
public class AjoutarticleController implements Initializable {

    @FXML
    private TextField sujet;
    @FXML
    private TextField titre;
    @FXML
    private TextField source;
    @FXML
    private DatePicker datecreation;
    @FXML
    private TextArea contenu;
    @FXML
    private Button btnpub;
    @FXML
    private Button btnretour;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void publier(ActionEvent event) throws Exception {
        String Sujet = sujet.getText();
        String Titre = titre.getText();
        LocalDate Datecreation = datecreation.getValue();
        Date daten = Date.valueOf(Datecreation);
        String Source = source.getText();
        String Contenu = contenu.getText();

        Article_agronomie_enfant A = new Article_agronomie_enfant(Sujet, daten, Source, Titre,Contenu);
        
       Article_agronomie_enfantCrud udao = Article_agronomie_enfantCrud.getInstance();
        udao.ajouterarticle(A);
        
        String mess = "aymen.negra@esprit.tn";
            sendmail.sendmail(mess);
        
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Ajout Article");
        alert.setContentText("Article a ete ajouté avec succes!");
        alert.showAndWait();
    }

    @FXML
    private void retour(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
            Stage stage = (Stage) btnretour.getScene().getWindow();
            stage.close();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(AjoutarticleController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
