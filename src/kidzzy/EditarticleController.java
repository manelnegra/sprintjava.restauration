/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kidzzy;

import Restauration.entities.Article_agronomie_enfant;
import Restauration.entities.Plat;
import crud.Article_agronomie_enfantCrud;
import crud.PlatCrud;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author WIKI
 */
public class EditarticleController implements Initializable {

    @FXML
    private TextField sujet;
    @FXML
    private TextField titre;
    @FXML
    private TextField source;
    @FXML
    private DatePicker datecreation;
    @FXML
    private TextArea contenu;
    @FXML
    private Button btnedit;
    @FXML
    private Button btnretour;
    
     String sujet_recup=AfficherarticlebController.sujetrecup;
       String source_recup= AfficherarticlebController.sourcerecup;
       String titre_recup= AfficherarticlebController.titrerecup;
       String contenu_recup= AfficherarticlebController.contenurecup;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        sujet.setText(sujet_recup);
        titre.setText(titre_recup);
        source.setText(source_recup);
        contenu.setText(contenu_recup);
    }

    @FXML
    private void edit(ActionEvent event) {
        String Sujet = sujet.getText();
        String Titre = titre.getText();
        String Source = source.getText();

        LocalDate Datearticle = datecreation.getValue();
        Date daten = Date.valueOf(Datearticle);
        String Contenu = contenu.getText();

        Article_agronomie_enfant p = new Article_agronomie_enfant(Sujet, daten, Titre, Source,Contenu);
        Article_agronomie_enfantCrud udao = Article_agronomie_enfantCrud.getInstance();
        udao.updateArticle(p);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Modification Plat");
        alert.setContentText("Plat a ete modifier avec succes!");

        alert.showAndWait();

    }

    @FXML
    private void retour(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
            Stage stage = (Stage) btnretour.getScene().getWindow();
            stage.close();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(EditplatController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

}
