/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kidzzy;

import Restauration.entities.Plat;
import crud.PlatCrud;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author WIKI
 */
public class AffichageController implements Initializable {

    @FXML
    private Button edit;
    @FXML
    private Button supp;
    @FXML
    private Button btnret;
    @FXML
    private TableView<Plat> tablev;
    @FXML
    private TableColumn<Plat, String> nom1;
    @FXML
    private TableColumn<Plat, Float> prix1;
    @FXML
    private TableColumn<Plat, Date> date1;
    @FXML
    private TableColumn<Plat, String> details1;

    private final ObservableList<Plat> datta = FXCollections.observableArrayList();
    @FXML
    private TextField rech;
    
     public static String nomrecup;
    public static Float prixrecup;
    public static String detailsrecup;

    private void settable() {

        PlatCrud pl = new PlatCrud();

        ArrayList<Plat> p = (ArrayList<Plat>) pl.displayPlat();
        ObservableList<Plat> obs = FXCollections.observableArrayList(p);
        tablev.setItems(obs);
        nom1.setCellValueFactory(new PropertyValueFactory<Plat, String>("nomPlat"));
        prix1.setCellValueFactory(new PropertyValueFactory<Plat, Float>("prixPlat"));
        date1.setCellValueFactory(new PropertyValueFactory<Plat, Date>("dateDispo"));
        details1.setCellValueFactory(new PropertyValueFactory<Plat, String>("detailsPlat"));
        datta.addAll(p);

    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        settable();
        FilteredList<Plat> filteredData = new FilteredList<>(datta, b -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        rech.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(Plat -> {
                // If filter text is empty, display all persons.

                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (Plat.getNomPlat().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                    return true; // Filter matches first name.
                }  else {
                    return false; // Does not match.
                }
            });
        });

        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<Plat> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        // 	  Otherwise, sorting the TableView would have no effect.
        sortedData.comparatorProperty().bind(tablev.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        tablev.setItems(sortedData);

    }
    

    @FXML
    private void editac(ActionEvent event) throws IOException {
       Plat p = tablev.getSelectionModel().getSelectedItem();

        AffichageController.nomrecup = p.getNomPlat();
        AffichageController.prixrecup = p.getPrixPlat();
        AffichageController.detailsrecup = p.getDetailsPlat();
       

        Parent root = FXMLLoader.load(getClass().getResource("editplat.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.hide();
        stage.setScene(scene);
        stage.show();  
        
        
    }

    @FXML
    private void suppac(ActionEvent event) {
        
       Plat p = tablev.getSelectionModel().getSelectedItem();
        PlatCrud udao = PlatCrud.getInstance();
        udao.delete(p);
    }

    @FXML
    private void retac(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
            Stage stage = (Stage) btnret.getScene().getWindow();
            stage.close();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(AffichageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
