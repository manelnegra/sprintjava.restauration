/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kidzzy;

import Restauration.entities.Plat;
import crud.PlatCrud;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author WIKI
 */
public class AjoutPlatController implements Initializable {

    @FXML
    private Button add;
    @FXML
    private TextField nomplat;
    @FXML
    private DatePicker dateplat;
    @FXML
    private TextField prixplat;
    @FXML
    private TextArea detail;
    @FXML
    private Button btnret;

    /**
     * Initializes the controller class.
     */
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void ajout(ActionEvent event) {
        String Nomplat = nomplat.getText();
        String Detail = detail.getText();
        float Prixplat = Float.parseFloat(prixplat.getText());
        LocalDate Dateplat = dateplat.getValue();
        Date daten = Date.valueOf(Dateplat);

        Plat p = new Plat(Nomplat, daten, Prixplat, Detail);
        
       PlatCrud udao = PlatCrud.getInstance();
        udao.ajouterplat(p);
        
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Ajout Plat");
        alert.setContentText("Plat a ete ajouté avec succes!");
        alert.showAndWait();
    }

    @FXML
    private void retour(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
            Stage stage = (Stage) btnret.getScene().getWindow();
            stage.close();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(AjoutPlatController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
