/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import kidzzyCnxDB.cnx;
import kidzzy.Ikidzzy.IPlat;
import Restauration.entities.Plat;

/**
 *
 * @author WIKI
 */
public class PlatCrud implements IPlat<Plat> {
    private static PlatCrud instance;
    private Statement st;
    private ResultSet rs;

    public PlatCrud() {
        cnx cs = cnx.getInstance();
        try {
            st = cs.getCnx().createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(PlatCrud.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static PlatCrud getInstance() {
        if (instance == null) {
            instance = new PlatCrud();
        }
        return instance;
    }
    @Override
    public void ajouterplat(Plat p) {
        String req = "insert into plat"
                + " (nomPlat,dateDispo,prixPlat,detailsPlat)"
                + " values ('" + p.getNomPlat()+ "','" + p.getDateDispo() + "','" + p.getPrixPlat() + "','" + p.getDetailsPlat()  + "')";

        try {
            st.executeUpdate(req);
        } catch (SQLException ex) {
            Logger.getLogger(PlatCrud.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public void delete(Plat p) {
        String req = "delete from plat where nomPlat ='" + p.getNomPlat()+"'";

        if (p != null) {
            try {

                st.executeUpdate(req);

            } catch (SQLException ex) {
                Logger.getLogger(Plat.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("n'existe pas");
        }
    }

    public boolean update(Plat p) {
         String qry = "UPDATE plat SET nomPlat = '" + p.getNomPlat() + "', dateDispo = '" + p.getDateDispo() + 
                 "', prixPlat = '" + p.getPrixPlat() + "', detailsPlat = '" + p.getDetailsPlat() + "' WHERE nomPlat = '" + p.getNomPlat()+"'";
        
        try {
            if (st.executeUpdate(qry) > 0) {
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(Plat.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public List<Plat> displayPlat() {
        List<Plat> myList = new ArrayList<>();
        cnx cs = cnx.getInstance();

        try {
            String requete = "select * from plat";
            Statement st = (Statement) cs.getCnx().createStatement();
            ResultSet rs = st.executeQuery(requete);

            while (rs.next()) {
                Plat p = new Plat();
                // p.setId_p(rs.getInt("id_p"));
                p.setNomPlat(rs.getString("nomPlat"));
                p.setDetailsPlat(rs.getString("detailsPlat"));

                p.setPrixPlat(rs.getFloat("prixPlat"));
  
                p.setDateDispo(rs.getDate("dateDispo"));
              

                myList.add(p);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return myList;
    }

}
