/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import kidzzyCnxDB.cnx;
import kidzzy.Ikidzzy.IArticle;
import Restauration.entities.Article_agronomie_enfant;

/**
 *
 * @author WIKI
 */
public class Article_agronomie_enfantCrud implements IArticle<Article_agronomie_enfant>{
    private static Article_agronomie_enfantCrud instance;
    private Statement st;
    private ResultSet rs;

    public Article_agronomie_enfantCrud() {
        cnx cs = cnx.getInstance();
        try {
            st = cs.getCnx().createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(Article_agronomie_enfantCrud.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Article_agronomie_enfantCrud getInstance() {
        if (instance == null) {
            instance = new Article_agronomie_enfantCrud();
        }
        return instance;
    }
    @Override
    public void ajouterarticle(Article_agronomie_enfant ar) {
       String req = "insert into article_agronomie_enfant"
                + " (SujetArticle,dateCreationArticle,sourceArticle,titreArticle,ContenuArticle)"
                + " values ('" + ar.getSujetArticle()+ "','" + ar.getDateCreationArticle() + "','" + ar.getSourceArticle() + "','" + ar.getTitreArticle()  + "','" + ar.getContenuArticle()  + "')";

        try {
            st.executeUpdate(req);
        } catch (SQLException ex) {
            Logger.getLogger(Article_agronomie_enfantCrud.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public void deleteArticle(Article_agronomie_enfant ar) {
        String req = "delete from article_agronomie_enfant where titreArticle='" + ar.getTitreArticle()+"'";

        if (ar != null) {
            try {

                st.executeUpdate(req);

            } catch (SQLException ex) {
                Logger.getLogger(Article_agronomie_enfant.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("n'existe pas");
        }
    }

    public boolean updateArticle(Article_agronomie_enfant ar) {
        String qry = "UPDATE Article_agronomie_enfant SET sujetArticle = '" + ar.getSujetArticle() + "', dateCreationArticle = '" + ar.getDateCreationArticle() + 
                 "', sourceArticle = '" + ar.getSourceArticle() + "', titreArticle = '" + ar.getTitreArticle() +"', ContenuArticle = '" + ar.getContenuArticle() + "' WHERE titreArticle = '" + ar.getContenuArticle()+"'";
        try {
            if (st.executeUpdate(qry) > 0) {
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(Article_agronomie_enfant.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public List<Article_agronomie_enfant> displayPlat() {
        List<Article_agronomie_enfant> myList = new ArrayList<>();
        cnx cs = cnx.getInstance();

        try {
            String requete = "select * from article_agronomie_enfant";
            Statement st = (Statement) cs.getCnx().createStatement();
            ResultSet rs = st.executeQuery(requete);

            while (rs.next()) {
                Article_agronomie_enfant ar = new Article_agronomie_enfant();
                ar.setSujetArticle(rs.getString("sujetArticle"));
                ar.setSourceArticle(rs.getString("sourceArticle"));
                ar.setTitreArticle(rs.getString("titreArticle"));
                ar.setContenuArticle(rs.getString("ContenuArticle"));
  
                ar.setDateCreationArticle(rs.getDate("dateCreationArticle"));
              

                myList.add(ar);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return myList;
    }
}
